package tshabalala.bongani.instacom.network;

import java.util.List;

import io.reactivex.Completable;

import io.reactivex.Single;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import tshabalala.bongani.instacom.model.Posts;

public interface PostDao {

    // Fetch all posts
    @GET("users/1/posts")
    Single<List<Posts>> fetchAllPosts();

    // Update single post
    @FormUrlEncoded
    @PUT("posts/{id}")
    Completable updatePost(@Path("id") int postId, @Field("Posts") Posts post);

    // Delete post
    @DELETE("posts/{id}")
    Completable deletePost(@Path("id") int postId);

    // Create post
    @FormUrlEncoded
    @POST("posts")
    Completable createPost(@Field("Posts") Posts posts);


}
