package tshabalala.bongani.instacom.model;

public class Posts {

    private int userID;
    private String title;
    private String body;
    private Posts posts;

    public Posts(int userID, String title, String body) {
        this.userID = userID;
        this.title = title;
        this.body = body;
    }

    public Posts(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public Posts() {
    }

    public int getId() {
        return userID;
    }

    public void setId(int userID) {
        this.userID = userID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Posts getPosts() {
        return posts;
    }

    public void setPosts(Posts posts) {
        this.posts = posts;
    }
}
