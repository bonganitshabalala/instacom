package tshabalala.bongani.instacom;


import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import tshabalala.bongani.instacom.adapter.PostAdapter;
import tshabalala.bongani.instacom.helper.MyDividerItemDecoration;
import tshabalala.bongani.instacom.helper.RecyclerTouchListener;
import tshabalala.bongani.instacom.helper.SwipeToDeleteCallback;
import tshabalala.bongani.instacom.model.Posts;
import tshabalala.bongani.instacom.network.ApiClient;
import tshabalala.bongani.instacom.network.PostDao;
import tshabalala.bongani.instacom.network.Util;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private CompositeDisposable disposable = new CompositeDisposable();
    private PostAdapter postsAdapter;
    private List<Posts> postList = new ArrayList<>();

    private PostDao postDao;

    @BindView(R.id.my_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        postDao = ApiClient.getClient(getApplicationContext()).create(PostDao.class);

        postsAdapter = new PostAdapter( postList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(postsAdapter);


        fab.setOnClickListener(view -> showPostDialog(false, null, -1));

        /**
         * On long press on RecyclerView item, open alert dialog
         * with options to choose
         * Edit and Delete
         * */
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            }

            @Override
            public void onLongClick(View view, int position) {
                showActionsDialog(position);
            }
        }));

        if(Util.isInternetAvailable(this)){

            fetchAllPosts();
        }else{
            Toast.makeText(MainActivity.this, R.string.internet_connection, Toast.LENGTH_LONG).show();
        }

        enableSwipeToDeleteAndUndo();

    }

    /**
     * Deleting a post
     * when swiping left.
     */
    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {


                final int position = viewHolder.getAdapterPosition();
                final Posts posts = postList.get(position);

                postsAdapter.removeItem(position);

                Snackbar snackbar = Snackbar.make(recyclerView, getString(R.string.enable_to_delete), Snackbar.LENGTH_LONG);
                snackbar.setAction("UNDO", view -> {

                    postsAdapter.restoreItem(posts, position);
                    recyclerView.scrollToPosition(position);
                });

                snackbar.setActionTextColor(Color.GREEN);
                snackbar.show();

            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            progressBar.setVisibility(View.VISIBLE);
            fetchAllPosts();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Fetching all posts from api
     * The received items will be in random order
     * map() operator is used to sort the items in descending order by Id
     */
    private void fetchAllPosts() {
        disposable.add(
                postDao.fetchAllPosts()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<List<Posts>>() {
                            @Override
                            public void onSuccess(List<Posts> notes) {
                                progressBar.setVisibility(View.GONE);
                                postList.clear();
                                postList.addAll(notes);
                                postsAdapter.notifyDataSetChanged();

                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError: " + e.getMessage());

                            }
                        })
        );
    }

    /**
     * Opens dialog with Edit - Delete options
     * Edit - 0
     * Delete - 0
     */
    private void showActionsDialog(final int position) {
        CharSequence options[] = new CharSequence[]{"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.choose));
        builder.setItems(options, (dialog, which) -> {
            if (which == 0) {
                showPostDialog(true, postList.get(position), position);
            } else {
                deletePost(postList.get(position).getId(), position);
            }
        });
        builder.show();
    }

    /**
     * Showing a Toast with error message
     * The error body will be in json format
     * {"error": "Error message!"}
     */
    private void showError(Throwable e) {
        String message = "";
        try {
            if (e instanceof IOException) {
                message = "No internet connection!";
            } else if (e instanceof HttpException) {
                HttpException error = (HttpException) e;
                assert error.response().errorBody() != null;
                String errorBody = error.response().errorBody().string();
                JSONObject jObj = new JSONObject(errorBody);

                message = jObj.getString("error");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        if (TextUtils.isEmpty(message)) {
            message = "Unknown error occurred! Check LogCat.";
        }

        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();

    }

    /**
     * Creating new post
     */
    private void createPost(Posts post) {
        disposable.add(
                postDao.createPost(post)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableCompletableObserver() {

                            @Override
                            public void onComplete() {
                                Log.d(TAG, "new note created: " + post.getId() + ", " + post.getTitle() + ", " + post.getBody());

                                // Add new item and notify adapter
                                postList.add(0, post);
                                postsAdapter.notifyItemInserted(0);

                                Toast.makeText(MainActivity.this, R.string.post_created, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError: " + e.getMessage());
                                showError(e);
                            }
                        }));
    }

    /**
     * Updating a post
     */
    private void updatePost(int postId, final Posts post, final int position) {
        disposable.add(
                postDao.updatePost(postId, post)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {
                                Log.d(TAG, "Post updated!");

                                Posts posts = postList.get(position);
                                posts.setPosts(post);

                                // Update item and notify adapter
                                postList.set(position, post);
                                postsAdapter.notifyItemChanged(position);

                                Toast.makeText(MainActivity.this, R.string.post_update, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError: " + e.getMessage());
                                showError(e);
                            }
                        }));
    }

    /**
     * Deleting a post
     */
    private void deletePost(final int postId, final int position) {
        Log.e(TAG, "deleteNote: " + postId + ", " + position);
        disposable.add(
                postDao.deletePost(postId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {
                                Log.d(TAG, "Post deleted! " + postId);

                                // Remove and notify adapter about item deletion
                                postList.remove(position);
                                postsAdapter.notifyItemRemoved(position);

                                Toast.makeText(MainActivity.this, R.string.post_delete, Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError: " + e.getMessage());
                                showError(e);
                            }
                        })
        );
    }

    /**
     * Shows alert dialog with EditText options to enter / edit
     * a post.
     * when shouldUpdate=true, it automatically displays old post and changes the
     * button text to UPDATE
     */
    private void showPostDialog(final boolean shouldUpdate, final Posts posts, final int position) {
        View view = LayoutInflater.from(this).inflate(R.layout.post_dialog, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText inputTitle = view.findViewById(R.id.title);
        final EditText inputBody = view.findViewById(R.id.body);
        TextView dialogTitle = view.findViewById(R.id.option);
        dialogTitle.setText(!shouldUpdate ? getString(R.string.edit_post_title) : getString(R.string.edit_post_body));

        if (shouldUpdate && posts != null) {
            inputTitle.setText(posts.getTitle());
            inputBody.setText(posts.getBody());
        }
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(shouldUpdate ? getString(R.string.update) : getString(R.string.save), (dialogBox, id) -> {

                })
                .setNegativeButton("cancel",
                        (dialogBox, id) -> dialogBox.cancel());

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            // Show toast message when no text is entered
            if (TextUtils.isEmpty(inputTitle.getText().toString())) {
                Toast.makeText(MainActivity.this, R.string.error_enter_title, Toast.LENGTH_SHORT).show();
                return;
            } else {
                alertDialog.dismiss();
            }

            if (TextUtils.isEmpty(inputBody.getText().toString())) {
                Toast.makeText(MainActivity.this, R.string.error_enter_body, Toast.LENGTH_SHORT).show();
                return;
            } else {
                alertDialog.dismiss();
            }

            Posts post = new Posts(inputTitle.getText().toString(), inputBody.getText().toString());
            // check if user updating post
            if (shouldUpdate && posts != null) {
                // update post by it's id

                updatePost(posts.getId(), post, position);
            } else {
                // create new note
                createPost(post);

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
