package tshabalala.bongani.instacom.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import tshabalala.bongani.instacom.R;
import tshabalala.bongani.instacom.model.Posts;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    private List<Posts> postsList;

    public PostAdapter(List<Posts> postsList) {
        this.postsList = postsList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.post_title)
        TextView postTitle;

        @BindView(R.id.post_body)
        TextView postBody;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.posts_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Posts post = postsList.get(position);

        holder.postTitle.setText(post.getTitle());
        holder.postBody.setText(post.getBody());

    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }


    public void removeItem(int position) {
        postsList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Posts posts, int position) {
        postsList.add(position, posts);
        notifyItemInserted(position);
    }

}